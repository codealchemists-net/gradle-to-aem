package org.gradle.jcr.vault.exception

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class ContentPackagingException extends Exception {

   public ContentPackagingException(String msg, Throwable ex){
        super(msg, ex)
    }

    public ContentPackagingException(String msg){
        super(msg)
    }

    public ContentPackagingException(Throwable ex){
        super(ex)
    }
}
