package org.gradle.jcr.vault.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.internal.file.IdentityFileResolver
import org.gradle.api.java.archives.internal.DefaultManifest
import org.gradle.jcr.vault.tasks.ContentPackagingTask
import org.gradle.jcr.vault.tasks.ContentUploadTask

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class ContentPackagePlugin implements Plugin<Project> {

    public void apply(Project project){
        project.extensions.create(ContentPackagePluginConstant.CONTENT_PACKAGE_PLUGIN_EXTENSION, ContentPackagePluginExtension)
        project.extensions.create(ContentPackagePluginConstant.CONTENT_PACKAGE_MANIFEST, ContentPackageManifestExtension)
        project.task(ContentPackagePluginConstant.CONTENT_PACKAGING_TASK, type: ContentPackagingTask)
        project.task(ContentPackagePluginConstant.CONTENT_PACKAGING_UPLOAD_TASK, type: ContentUploadTask)
    }
}
