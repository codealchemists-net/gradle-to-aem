package org.gradle.jcr.vault.plugin

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
final class ContentPackagePluginConstant {

    public static final String CONTENT_PACKAGE_PLUGIN_EXTENSION = "cppExtension"

    public static final String CONTENT_PACKAGE_MANIFEST = "packageManifest"

    public static final String CONTENT_PACKAGING_TASK = "content-package"

    public static final String CONTENT_PACKAGING_UPLOAD_TASK ="installPackage"
}
