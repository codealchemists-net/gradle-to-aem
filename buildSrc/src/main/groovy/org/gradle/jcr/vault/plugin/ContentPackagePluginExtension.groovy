package org.gradle.jcr.vault.plugin

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class ContentPackagePluginExtension {

    def List<Map<String, String>> embeddeds

    def List<Map<String, String>> subPackages

    def String builtContentDirectory

    def String filterSource

    def String filters

    def String group

    def String outputDirectory

    def String version

    def String workDirectory

    def String artifactId

    def String finalName

    def String prefix

    def String targetURL
}

