package org.gradle.jcr.vault.support

import org.codehaus.plexus.archiver.jar.JarArchiver

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class ContentPackageArchiver extends JarArchiver
{
    public ContentPackageArchiver()
    {
        this.archiveType = "content-package";
    }
}