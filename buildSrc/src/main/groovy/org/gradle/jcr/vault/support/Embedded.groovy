package org.gradle.jcr.vault.support

import org.gradle.api.Project
import org.gradle.api.file.FileTree
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.spi.LoggerFactoryBinder

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class Embedded {

 private final static Logger log = LoggerFactory.getLogger(Embedded.class);
    private final String groupId

    private final String artifactId
    private String scope;
    private String type;
    private String classifier;
    private boolean filter;
    private String target;
    private String destFileName;
    private boolean excludeTransitive;

    Embedded(Map<String, String> entry){
        log.error("Embedded with - " + entry)
        this.type = entry.type
        this.filter = false
        if(null != entry.filter) {
            this.filter = Boolean.getBoolean(entry.filter)
        }

        this.target = entry.target
        this.scope = entry.scope
        this.groupId = entry.groupId
        this.artifactId = entry.artifactId
        this.classifier = entry.classifier
        this.destFileName = entry.destFileName
        this.excludeTransitive = false
        if(null != entry.excludeTransitive){
            this.excludeTransitive = Boolean.getBoolean(entry.excludeTransitive)
        }

        if (null != target && !target.endsWith("/")) {
            target = target + "/";
        }

    }
    public boolean isFilter() {
        return this.filter;
    }

    public String getDestFileName() {
        return this.destFileName;
    }

    public String getTarget() {
        return this.target;
    }

    public boolean isExcludeTransitive() {
        return this.excludeTransitive;
    }

    public List<File> getMatchingArtifacts(Project project)
    {
        List matches = new ArrayList()
        log.error("searching for artifactId on path - "+artifactId+" directory - "+ project.rootProject.projectDir.absolutePath+"/"+artifactId)
        def FileTree fileTree = project.fileTree(project.rootProject.projectDir.absolutePath+"/"+artifactId).matching { include("**/*"+artifactId+"*.jar")}
        fileTree.each {File file ->
            matches.add(file)
log.error("found file - "+ file)
        }
        return matches;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Embedded: ");
        if (this.groupId != null) {
            builder.append("groupId=").append(this.groupId).append(",");
        }
        if (this.artifactId != null) {
            builder.append("artifactId=").append(this.artifactId).append(",");
        }
        if (this.scope != null) {
            builder.append("scope=").append(this.scope).append(",");
        }
        builder.append("filter=").append(this.filter);
        builder.append(",excludeTransitive=").append(this.excludeTransitive);
        if (this.target != null) {
            builder.append(",target=").append(this.target);
        }
        if (this.destFileName != null) {
            builder.append(",destFileName=").append(this.destFileName);
        }
        return builder.toString();
    }
}
