package org.gradle.jcr.vault.support

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class PackageId implements Comparable<PackageId>
{
    public static final String ETC_PACKAGES = "/etc/packages";
    public static final String ETC_PACKAGES_PREFIX = "/etc/packages/";
    public static final PackageId[] EMPTY = new PackageId[0];
    private final String group;
    private final String name;
    private final Version version;
    private final String str;
    private final boolean fromPath;

    public PackageId(String path)
    {
        this.fromPath = true;
        path = path.trim();
        int idx = path.lastIndexOf('.');
        if (idx > 0) {
            String ext = path.substring(idx);
            if ((ext.equalsIgnoreCase(".zip")) || (ext.equalsIgnoreCase(".jar"))) {
                path = path.substring(0, idx);
            }
        }
        idx = path.lastIndexOf('/');
        String name;
        if (idx < 0) {
            name = path;
            this.group = "";
        } else {
            name = path.substring(idx + 1);
            String grp = path.substring(0, idx);
            if (grp.equals("/etc/packages"))
                grp = "";
            else if (grp.startsWith("/etc/packages/"))
                grp = grp.substring("/etc/packages/".length());
            else if (grp.startsWith("/")) {
                grp = grp.substring(1);
            }
            this.group = grp;
        }

        String[] segs = name.split("-");
        int i = segs.length - 1;
        while (i > 0)
        {
            try {
                if (Integer.parseInt(segs[i]) >= 1000) {
                    break;
                }
            }
            catch (NumberFormatException e)
            {
            }
            if ((Character.isJavaIdentifierStart(segs[i].charAt(0))) && (
                    (segs[i].length() == 1) || ((!Character.isDigit(segs[i].charAt(1))) && (!segs[i].equals("SNAPSHOT")))))
            {
                break;
            }
            i--;
        }
        if (i == segs.length - 1) {
            this.name = name;
            this.version = Version.EMPTY;
        } else {
            StringBuilder str = new StringBuilder();
            for (int j = 0; j <= i; j++) {
                if (j > 0) {
                    str.append('-');
                }
                str.append(segs[j]);
            }
            this.name = str.toString();
            str.setLength(0);
            for (int j = i + 1; j < segs.length; j++) {
                if (j > i + 1) {
                    str.append('-');
                }
                str.append(segs[j]);
            }
            this.version = Version.create(str.toString());
        }
        this.str = getString(this.group, this.name, this.version);
    }

    public PackageId(String path, String version)
    {
        this(path, Version.create(version));
    }

    public PackageId(String path, Version version)
    {
        this.fromPath = true;
        path = path.trim();
        int idx = path.lastIndexOf('.');
        if (idx > 0) {
            String ext = path.substring(idx);
            if ((ext.equalsIgnoreCase(".zip")) || (ext.equalsIgnoreCase(".jar"))) {
                path = path.substring(0, idx);
            }
        }
        if ((version != null) && (path.endsWith('-' + version.toString()))) {
            path = path.substring(0, path.length() - version.toString().length() - 1);
        }
        idx = path.lastIndexOf('/');
        if (idx < 0) {
            this.name = path;
            this.group = "";
        } else {
            this.name = path.substring(idx + 1);
            String grp = path.substring(0, idx);
            if (grp.equals("/etc/packages"))
                grp = "";
            else if (grp.startsWith("/etc/packages/"))
                grp = grp.substring("/etc/packages/".length());
            else if (grp.startsWith("/")) {
                grp = grp.substring(1);
            }
            this.group = grp;
        }

        if ((version == null) || (version.toString().length() == 0)) {
            version = Version.EMPTY;
        }
        this.version = version;
        this.str = getString(this.group, this.name, version);
    }

    public PackageId(String group, String name, String version)
    {
        this(group, name, Version.create(version));
    }

    public PackageId(String group, String name, Version version)
    {
        this.fromPath = false;

        if (group.equals("/etc/packages"))
            group = "";
        else if (group.startsWith("/etc/packages/"))
            group = group.substring("/etc/packages/".length());
        else if (group.startsWith("/")) {
            group = group.substring(1);
        }
        this.group = group;
        this.name = name;
        this.version = (version == null ? Version.EMPTY : version);
        this.str = getString(this.group, name, this.version);
    }

    public static PackageId fromString(String str)
    {
        if ((str == null) || (str.length() == 0)) {
            return null;
        }
        String[] segs = str.split(":");
        if (segs.length == 1)
            return new PackageId("", segs[0], "");
        if (segs.length == 2) {
            return new PackageId(segs[0], segs[1], "");
        }
        return new PackageId(segs[0], segs[1], segs[2]);
    }

    public static PackageId[] fromString(String[] str)
    {
        PackageId[] ret = new PackageId[str.length];
        for (int i = 0; i < str.length; i++) {
            ret[i] = fromString(str[i]);
        }
        return ret;
    }

    public static String toString(PackageId[] packs)
    {
        String delim = "";
        StringBuilder b = new StringBuilder();
        for (PackageId pack : packs) {
            b.append(delim).append(pack);
            delim = ",";
        }
        return b.toString();
    }

    /** @deprecated */
    public String getPath()
    {
        return getInstallationPath();
    }

    public boolean isFromPath()
    {
        return this.fromPath;
    }

    public String getInstallationPath()
    {
        StringBuilder b = new StringBuilder("/etc/packages/");
        if (this.group.length() > 0) {
            b.append(this.group);
            b.append("/");
        }
        b.append(this.name);
        if (this.version.toString().length() > 0) {
            b.append("-").append(this.version);
        }
        return b.toString();
    }

    /** @deprecated */
    public String getGroupId()
    {
        return this.group;
    }

    public String getGroup()
    {
        return this.group;
    }

    public String getName()
    {
        return this.name;
    }

    public String getVersionString()
    {
        return this.version.toString();
    }

    public String getDownloadName()
    {
        StringBuilder str = new StringBuilder(this.name);
        if (this.version.toString().length() > 0) {
            str.append("-").append(this.version);
        }
        str.append(".zip");
        return str.toString();
    }

    public Version getVersion()
    {
        return this.version;
    }

    public String toString()
    {
        return this.str;
    }

    public boolean equals(Object o)
    {
        return (this == o) || (((o instanceof PackageId)) && (this.str.equals(o.toString())));
    }

    public int hashCode()
    {
        return this.str.hashCode();
    }

    public int compareTo(PackageId o)
    {
        int comp = this.group.compareTo(o.getGroup());
        if (comp != 0) {
            return comp;
        }
        comp = this.name.compareTo(o.getName());
        if (comp != 0) {
            return comp;
        }
        return this.version.compareTo(o.getVersion());
    }

    private static String getString(String group, String name, Version version)
    {
        return getString(group, name, version == null ? "" : version.toString());
    }

    private static String getString(String group, String name, String version)
    {
        StringBuilder b = new StringBuilder();
        b.append(group).append(':');
        b.append(name);
        if (version.length() > 0) {
            b.append(':').append(version);
        }
        return b.toString();
    }
}