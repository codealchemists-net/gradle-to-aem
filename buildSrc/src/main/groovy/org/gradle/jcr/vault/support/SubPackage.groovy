package org.gradle.jcr.vault.support

import org.gradle.api.Project
import org.gradle.api.file.FileTree

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class SubPackage {

    private final String groupId

    private final String artifactId
    private String scope;
    private String type;
    private String classifier;
    private boolean filter;
    private String target;
    private String destFileName;
    private boolean excludeTransitive;

    SubPackage(Map<String, String> entry){
        this.type = entry.type
        this.filter = false
        if(null != entry.filter) {
            this.filter = Boolean.getBoolean(entry.filter)
        }

        this.target = entry.target
        this.scope = entry.scope
        this.groupId = entry.groupId
        this.artifactId = entry.artifactId
        this.classifier = entry.classifier
        this.destFileName = entry.destFileName
        this.excludeTransitive = false
        if(null != entry.excludeTransitive){
            this.excludeTransitive = Boolean.getBoolean(entry.excludeTransitive)
        }

        if (null != target && !target.endsWith("/")) {
            target = target + "/";
        }

    }
    public boolean isFilter() {
        return this.filter;
    }

    public String getDestFileName() {
        return this.destFileName;
    }

    public String getTarget() {
        return this.target;
    }

    public boolean isExcludeTransitive() {
        return this.excludeTransitive;
    }

    public List<File> getMatchingArtifacts(Project project)
    {
        List matches = new ArrayList()
        def FileTree fileTree = project.rootProject.fileTree().matching { include("*$artifactId*")}
        fileTree.each {File file ->
            matches.add(file)

        }
        return matches;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Embedded: ");
        if (this.groupId != null) {
            builder.append("groupId=").append(this.groupId).append(",");
        }
        if (this.artifactId != null) {
            builder.append("artifactId=").append(this.artifactId).append(",");
        }
        if (this.scope != null) {
            builder.append("scope=").append(this.scope).append(",");
        }
        builder.append("filter=").append(this.filter);
        builder.append(",excludeTransitive=").append(this.excludeTransitive);
        if (this.target != null) {
            builder.append(",target=").append(this.target);
        }
        if (this.destFileName != null) {
            builder.append(",destFileName=").append(this.destFileName);
        }
        return builder.toString();
    }
}
