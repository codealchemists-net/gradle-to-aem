package org.gradle.jcr.vault.support

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class Version implements Comparable<Version>
{
    public static final Version EMPTY = new Version("", new String[0]);
    private final String str;
    private final String[] segments;

    /** @deprecated */
    public Version(String str)
    {
        this(str, str.split("\\."));
    }

    public static Version create(String str)
    {
        if ((str == null) || (str.length() == 0)) {
            return EMPTY;
        }
        return new Version(str, str.split("\\."));
    }

    public static Version create(String[] segments)
    {
        if ((segments == null) || (segments.length == 0)) {
            return EMPTY;
        }
        StringBuilder b = new StringBuilder();
        String delim = "";
        for (String s : segments) {
            b.append(delim);
            b.append(s);
            delim = ".";
        }
        return new Version(b.toString(), segments);
    }

    private Version(String str, String[] segments)
    {
        if (str == null) {
            throw new NullPointerException("Version String must not be null.");
        }
        this.str = str;
        this.segments = segments;
    }

    public int hashCode()
    {
        return this.str.hashCode();
    }

    public boolean equals(Object o)
    {
        return (this == o) || (((o instanceof Version)) && (this.str.equals(((Version)o).str)));
    }

    public String toString()
    {
        return this.str;
    }

    public String[] getNormalizedSegments()
    {
        return this.segments;
    }

    public int compareTo(Version o)
    {
        String[] oSegs = o.getNormalizedSegments();
        for (int i = 0; i < Math.min(this.segments.length, oSegs.length); i++) {
            String s1 = this.segments[i];
            String s2 = oSegs[i];
            if (!s1.equals(s2))
            {
                try
                {
                    int v1 = Integer.parseInt(this.segments[i]);
                    int v2 = Integer.parseInt(oSegs[i]);
                    if (v1 != v2)
                        return v1 - v2;
                }
                catch (NumberFormatException e)
                {
                }
                String[] ss1 = s1.split("-");
                String[] ss2 = s2.split("-");
                for (int j = 0; j < Math.min(ss1.length, ss2.length); j++) {
                    String c1 = ss1[j];
                    String c2 = ss2[j];
                    try {
                        int v1 = Integer.parseInt(c1);
                        int v2 = Integer.parseInt(c2);
                        if (v1 != v2)
                            return v1 - v2;
                    }
                    catch (NumberFormatException e)
                    {
                    }
                    int c = c1.compareTo(c2);
                    if (c != 0) {
                        return c;
                    }
                }
                int c = ss1.length - ss2.length;
                if (c != 0)
                    return -c;
            }
        }
        return this.segments.length - oSegs.length;
    }
}