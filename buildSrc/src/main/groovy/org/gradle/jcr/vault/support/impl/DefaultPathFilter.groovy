package org.gradle.jcr.vault.support.impl

import java.util.regex.Pattern

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class DefaultPathFilter
        implements PathFilter
{
    private Pattern regex;

    public DefaultPathFilter()
    {
    }

    public DefaultPathFilter(String pattern)
    {
        setPattern(pattern);
    }

    public void setPattern(String pattern)
    {
        this.regex = Pattern.compile(pattern);
    }

    public String getPattern()
    {
        return this.regex.pattern();
    }

    public boolean matches(String path)
    {
        return this.regex.matcher(path).matches();
    }

    public boolean isAbsolute()
    {
        return this.regex.pattern().startsWith("/");
    }
}
