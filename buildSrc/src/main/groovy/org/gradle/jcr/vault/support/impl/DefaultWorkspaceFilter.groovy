package org.gradle.jcr.vault.support.impl

import org.codehaus.plexus.util.IOUtil
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException
import org.codehaus.plexus.util.xml.pull.MXSerializer
import org.codehaus.plexus.util.xml.pull.XmlSerializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.xml.sax.SAXException

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
public class DefaultWorkspaceFilter
{
    private static final Logger log = LoggerFactory.getLogger(DefaultWorkspaceFilter.class);

    private final List<PathFilterSet> filterSets = new LinkedList();
    public static final String ATTR_VERSION = "version";
    public static final double SUPPORTED_VERSION = 1.0D;
    protected double version = 1.0D;
    private byte[] source;
    private PathFilter globalIgnored;

    public void add(PathFilterSet set)
    {
        this.filterSets.add(set);
    }

    public List<PathFilterSet> getFilterSets() {
        return this.filterSets;
    }

    public PathFilterSet getCoveringFilterSet(String path) {
        if (isGloballyIgnored(path)) {
            return null;
        }
        for (PathFilterSet set : this.filterSets) {
            if (set.covers(path)) {
                return set;
            }
        }
        return null;
    }

    public boolean contains(String path) {
        if (isGloballyIgnored(path)) {
            return false;
        }
        for (PathFilterSet set : this.filterSets) {
            if (set.contains(path)) {
                return true;
            }
        }
        return false;
    }

    public boolean covers(String path) {
        if (isGloballyIgnored(path)) {
            return false;
        }
        for (PathFilterSet set : this.filterSets) {
            if (set.covers(path)) {
                return true;
            }
        }
        return false;
    }

    public boolean isAncestor(String path) {
        for (PathFilterSet set : this.filterSets) {
            if (set.isAncestor(path)) {
                return true;
            }
        }
        return false;
    }

    public boolean isGloballyIgnored(String path) {
        return (this.globalIgnored != null) && (this.globalIgnored.matches(path));
    }



    public void load(File file)
            throws IOException
    {
        load(new FileInputStream(file));
    }

    public InputStream getSource() {
        if (this.source == null) {
            generateSource();
        }
        return new ByteArrayInputStream(this.source);
    }

    public String getSourceAsString() {
        if (this.source == null)
            generateSource();
        try
        {
            return new String(this.source, "utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    public void load(InputStream inputStream)
    throws IOException
    {
        try
        {
            this.source = IOUtil.toByteArray(inputStream);
            inputStream = getSource();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);

            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(inputStream);
            Element doc = document.getDocumentElement();
            if (!doc.getNodeName().equals("workspaceFilter")) {
                throw new IOException("<workspaceFilter> expected.");
            }
            String v = doc.getAttribute("version");
            if ((v == null) || (v.equals(""))) {
                v = "1.0";
            }
            this.version = Double.parseDouble(v);
            if (this.version > 1.0D) {
                throw new IOException("version " + this.version + " not supported.");
            }
            read(doc);
        } catch (ParserConfigurationException e) {
            IOException ioe = new IOException("Unable to create configuration XML parser");
            e.initCause(e);
            throw ioe;
        } catch (SAXException e) {
            IOException ioe = new IOException("Configuration file syntax error.");
            e.initCause(e);
            throw ioe;
        } finally {
            IOUtil.close(inputStream);
        }
    }

    private void read(Element elem) throws IOException
    {
        NodeList nl = elem.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            Node child = nl.item(i);
            if (child.getNodeType() == 1) {
                if (!child.getNodeName().equals("filter")) {
                    throw new IOException("<filter> expected.");
                }
                PathFilterSet pathFilterSet = readDef((Element)child);
                this.filterSets.add(pathFilterSet);
            }
        }
    }

    private PathFilterSet readDef(Element elem) throws IOException {
        String root = elem.getAttribute("root");
        PathFilterSet pathFilterSet = new PathFilterSet((root == null) || (root.length() == 0) ? "/" : root);

        String mode = elem.getAttribute("mode");
        if ((mode != null) && (mode.length() > 0)) {
            pathFilterSet.setImportMode(PackageImportMode.valueOf(mode.toUpperCase()));
        }

        NodeList n1 = elem.getChildNodes();
        for (int i = 0; i < n1.getLength(); i++) {
            Node child = n1.item(i);
            if (child.getNodeType() == 1) {
                if (child.getNodeName().equals("include"))
                    pathFilterSet.addInclude(readFilter((Element)child))
                else if (child.getNodeName().equals("exclude"))
                    pathFilterSet.addExclude(readFilter((Element)child))
                else {
                    throw new IOException("either <include> or <exclude> expected.")
                }
            }
        }
        return pathFilterSet
    }

    private PathFilter readFilter(Element elem) throws IOException {
        String pattern = elem.getAttribute("pattern");
        if ((pattern == null) || (pattern.equals(""))) {
            throw new IOException("Filter pattern must not be empty");
        }
        return new DefaultPathFilter(pattern);
    }

    private void generateSource() {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            XmlSerializer ser = new MXSerializer();
            ser.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "    ");
            ser.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
            ser.setOutput(out, "UTF-8");
            ser.startDocument("UTF-8", null);
            ser.text("\n");
            ser.startTag(null, "workspaceFilter");
            ser.attribute(null, "version", String.valueOf(this.version));
            for (PathFilterSet set : this.filterSets)
            {
                ser.startTag(null, "filter");

                ser.attribute(null, "root", set.getRoot());
                if (set.getImportMode() != PackageImportMode.REPLACE)
                {
                    ser.attribute(null, "mode", set.getImportMode().name().toLowerCase());
                }

                for (FilterSet.Entry entry : set.getEntries())
                {
                    PathFilter filter = (PathFilter)entry.getFilter();
                    if ((filter instanceof DefaultPathFilter)) {
                        if (entry.isInclude()) {
                            ser.startTag(null, "include");
                            ser.attribute(null, "pattern", ((DefaultPathFilter)filter).getPattern());
                            ser.endTag(null, "include");
                        } else {
                            ser.startTag(null, "exclude");
                            ser.attribute(null, "pattern", ((DefaultPathFilter)filter).getPattern());
                            ser.endTag(null, "exclude");
                        }
                    }
                    else throw new IllegalArgumentException("Can only export default path filters, yet.");
                }

                ser.endTag(null, "filter");
            }
            ser.endTag(null, "workspaceFilter");
            ser.endDocument();
            this.source = out.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void setGlobalIgnored(PathFilter ignored) {
        this.globalIgnored = ignored;
    }

    public void merge(DefaultWorkspaceFilter source) {
        if(null == source){
            return
        }
        for (PathFilterSet fs : source.getFilterSets())
        {
            for (PathFilterSet mfs : getFilterSets()) {
                if (mfs.getRoot().equals(fs.getRoot())) {
                    throw new IllegalArgumentException("Merging of equal filter roots not allowed for: " + fs.getRoot());
                }
            }
            add(fs);
        }
    }
}
