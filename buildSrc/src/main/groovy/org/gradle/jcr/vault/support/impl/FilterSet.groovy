package org.gradle.jcr.vault.support.impl

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
public abstract class FilterSet<E extends Filter>
{
    private String root;
    private String rootPattern;
    private List<Entry<E>> entries;
    private boolean sealed;
    private PackageImportMode mode = PackageImportMode.REPLACE;

    public FilterSet()
    {
        this("");
    }

    public FilterSet(String root)
    {
        setRoot(root);
    }

    public String getRoot()
    {
        return this.root.equals("") ? "/" : this.root;
    }

    public void setRoot(String path)
    {
        if (this.sealed) {
            throw new UnsupportedOperationException("FilterSet is sealed.");
        }
        if (path.endsWith("/")) {
            this.rootPattern = path;
            this.root = path.substring(0, path.length() - 1);
        } else {
            this.rootPattern = (path + "/");
            this.root = path;
        }
    }

    public PackageImportMode getImportMode()
    {
        return this.mode;
    }

    public void setImportMode(PackageImportMode mode)
    {
        if (this.sealed) {
            throw new UnsupportedOperationException("FilterSet is sealed.");
        }
        this.mode = mode;
    }

    public FilterSet seal()
    {
        if (!this.sealed) {
            if (this.entries == null)
                this.entries = Collections.emptyList();
            else {
                this.entries = Collections.unmodifiableList(this.entries);
            }
            this.sealed = true;
        }
        return this;
    }

    public boolean isSealed()
    {
        return this.sealed;
    }

    public FilterSet addAll(FilterSet<E> set)
    {
        if (this.sealed) {
            throw new UnsupportedOperationException("FilterSet is sealed.");
        }
        if (this.entries == null) {
            this.entries = new LinkedList(set.entries);
        } else {
            this.entries.clear();
            this.entries.addAll(set.entries);
        }
        return this;
    }

    public FilterSet addInclude(E filter)
    {
        addEntry(new Entry(filter, true));
        return this;
    }

    public FilterSet addExclude(E filter)
    {
        addEntry(new Entry(filter, false));
        return this;
    }

    private void addEntry(Entry<E> e)
    {
        if (this.sealed) {
            throw new UnsupportedOperationException("FilterSet is sealed.");
        }
        if (this.entries == null) {
            this.entries = new LinkedList();
        }
        this.entries.add(e);
    }

    public List<Entry<E>> getEntries()
    {
        seal();
        return this.entries;
    }

    public boolean isEmpty()
    {
        return (this.entries == null) || (this.entries.isEmpty());
    }

    public boolean covers(String path)
    {
        return (path.equals(this.root)) || (path.startsWith(this.rootPattern));
    }

    public boolean isAncestor(String path)
    {
        return (path.equals(this.root)) || (this.root.startsWith(path + "/")) || (path.equals("/"));
    }

    public int hashCode()
    {
        return 0;
    }

    public boolean equals(Object obj)
    {
        if (this == obj) {
            return true;
        }
        if ((obj instanceof FilterSet)) {
            return this.entries.equals(((FilterSet)obj).entries);
        }
        return false;
    }

    public static class Entry<E extends Filter>
    {
        protected final E filter;
        protected final boolean include;

        public Entry(E filter, boolean include)
        {
            this.filter = filter;
            this.include = include;
        }

        public E getFilter()
        {
            return this.filter;
        }

        public boolean isInclude()
        {
            return this.include;
        }

        public int hashCode()
        {
            return 0;
        }

        public boolean equals(Object obj)
        {
            if (this == obj) {
                return true;
            }
            if ((obj instanceof Entry)) {
                return (((Entry)obj).include == this.include) && (((Entry)obj).filter.equals(this.filter));
            }
            return false;
        }
    }
}
