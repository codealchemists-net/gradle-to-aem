package org.gradle.jcr.vault.support.impl

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
enum PackageImportMode {
        REPLACE ,MERGE,UPDATE
}
