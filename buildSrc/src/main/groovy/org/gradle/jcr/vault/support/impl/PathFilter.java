package org.gradle.jcr.vault.support.impl;

/**
 * Created by ameesh.trikha on 31/12/2014.
 */
public abstract interface PathFilter extends Filter
{
    public static final PathFilter ALL = new PathFilter()
    {
        public boolean matches(String path)
        {
            return true;
        }

        public boolean isAbsolute()
        {
            return true;
        }
    };

    public static final PathFilter NONE = new PathFilter()
    {
        public boolean matches(String path)
        {
            return false;
        }

        public boolean isAbsolute()
        {
            return true;
        }
    };

    public abstract boolean matches(String paramString);

    public abstract boolean isAbsolute();
}