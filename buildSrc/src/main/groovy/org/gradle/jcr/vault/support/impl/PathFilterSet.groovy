package org.gradle.jcr.vault.support.impl

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class PathFilterSet extends FilterSet<PathFilter>
{
    public static final PathFilterSet INCLUDE_ALL = (PathFilterSet)new PathFilterSet().addInclude(PathFilter.ALL).seal();

    public static final PathFilterSet EXCLUDE_ALL = (PathFilterSet)new PathFilterSet().addExclude(PathFilter.ALL).seal();
    private boolean onlyRelativePatterns;

    public PathFilterSet()
    {
    }

    public PathFilterSet(String root)
    {
        super(root);
    }

    public boolean contains(String path)
    {
        if (!covers(path)) {
            return false;
        }
        List entries = getEntries();
        if (entries.isEmpty()) {
            return true;
        }
        boolean result = !((FilterSet.Entry)entries.get(0)).include;
        for (FilterSet.Entry entry : entries) {
            if (((PathFilter)entry.filter).matches(path)) {
                result = entry.include;
            }
        }
        return result;
    }

    public FilterSet seal()
    {
        if (!isSealed()) {
            super.seal();
            this.onlyRelativePatterns = true;
            for (FilterSet.Entry entry : getEntries()) {
                if ((!entry.include) || (((PathFilter)entry.filter).isAbsolute())) {
                    this.onlyRelativePatterns = false;
                    break;
                }
            }
        }
        return this;
    }

    public boolean hasOnlyRelativePatterns()
    {
        seal();
        return this.onlyRelativePatterns;
    }
}
