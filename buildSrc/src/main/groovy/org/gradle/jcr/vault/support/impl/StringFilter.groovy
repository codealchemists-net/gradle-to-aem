package org.gradle.jcr.vault.support.impl

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class StringFilter extends DefaultPathFilter
{
    private String string;

    public StringFilter(String pattern)
    {
        super(pattern);
    }

    public String getPattern()
    {
        if (this.string == null) {
            return super.getPattern();
        }
        return this.string;
    }

    public void setPattern(String pattern)
    {
        if (pattern.startsWith("/")) {
            pattern = pattern.substring(1);
            if (pattern.endsWith("/")) {
                pattern = pattern.substring(0, pattern.length() - 1);
            }
            super.setPattern(pattern);
        } else {
            this.string = pattern;
        }
    }

    public boolean matches(String path)
    {
        if (this.string == null) {
            return super.matches(path);
        }
        return this.string.equals(path);
    }

    public String toString()
    {
        if (this.string == null) {
            return "/" + getPattern() + "/";
        }
        return getPattern();
    }
}
