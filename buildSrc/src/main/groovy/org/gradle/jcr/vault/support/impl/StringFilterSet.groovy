package org.gradle.jcr.vault.support.impl

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class StringFilterSet extends FilterSet<StringFilter>
{
    public void addEntry(String pattern)
    {
        if (pattern.startsWith("~"))
            addExclude(new StringFilter(pattern.substring(1)));
        else
            addInclude(new StringFilter(pattern));
    }

    public void addEntries(String patterns)
    {
        for (String name : patterns.split(","))
            addEntry(name.trim());
    }

    public boolean contains(String path)
    {
        List entries = getEntries();
        if (entries.isEmpty()) {
            return true;
        }
        boolean result = !((FilterSet.Entry)entries.get(0)).isInclude();
        for (FilterSet.Entry entry : entries) {
            if (((StringFilter)entry.getFilter()).matches(path)) {
                result = entry.isInclude();
            }
        }
        return result;
    }

    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        String delim = "";
        for (FilterSet.Entry entry : getEntries()) {
            builder.append(delim);
            if (!entry.isInclude()) {
                builder.append("~");
            }
            builder.append(entry.getFilter());
            delim = ",";
        }
        return builder.toString();
    }
}