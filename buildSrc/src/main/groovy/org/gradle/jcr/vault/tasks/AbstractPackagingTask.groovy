package org.gradle.jcr.vault.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.gradle.jcr.vault.exception.ContentPackagingException
import org.gradle.jcr.vault.plugin.ContentPackagePluginConstant
import org.gradle.jcr.vault.plugin.ContentPackagePluginExtension
import org.gradle.jcr.vault.support.ContentPackageArchiver
import org.gradle.jcr.vault.support.Embedded
import org.gradle.jcr.vault.support.PackageId
import org.gradle.jcr.vault.support.SubPackage
import org.gradle.jcr.vault.support.impl.DefaultWorkspaceFilter
import org.gradle.jcr.vault.support.impl.PathFilterSet
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.codehaus.plexus.util.FileUtils
import org.codehaus.plexus.util.IOUtil

import java.text.DateFormat
import java.text.SimpleDateFormat
import org.codehaus.plexus.archiver.jar.Manifest
import java.util.zip.ZipEntry
import java.util.zip.ZipFile

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
abstract class AbstractPackagingTask extends DefaultTask{

    private static final Logger log = LoggerFactory.getLogger(AbstractPackagingTask.class)

    def String prefix
    def String workDirectory
    def String outputDirectory
    def String finalName
    private final DefaultWorkspaceFilter filters = new DefaultWorkspaceFilter();

    ContentPackagePluginExtension getExtension(){
        ContentPackagePluginExtension  cppExtension = (ContentPackagePluginExtension) project.extensions.getByName("cppExtension")

        return cppExtension
    }
    String getExtensionPropAsString(String propertyName) {
        Object property = getExtension().getProperty(propertyName);
        if(null!=property){
            return (String) getExtension().getProperty(propertyName);
        }
        return null;
    }

    boolean getExtensionPropAsBoolean(String propertyName){
        Object property = getExtension().getProperty(propertyName);
        if(null!=property){
            return (Boolean) getExtension().getProperty(propertyName);
        }
        return false;
    }
@TaskAction
    protected void buildPackage(){
    log.error("project.buildDir.absolutePath" + project.buildDir.absolutePath)
        prefix = getExtensionPropAsString("prefix")
        workDirectory = project.buildDir.absolutePath+"/"+getExtensionPropAsString("workDirectory")
        outputDirectory = project.buildDir.absolutePath
        finalName = getExtensionPropAsString("finalName")
        if (prefix == null)
            prefix = "";
        else if (!prefix.endsWith("/")) {
            prefix += "/";
        }



        File vaultDir = new File(workDirectory, "META-INF/vault");
        File definitionDir = new File(vaultDir, "definition");
        File finalFile = new File(outputDirectory, finalName + ".zip");
        try
        {
            vaultDir.mkdirs();
            definitionDir.mkdirs();

            Map embeddedFiles = copyEmbeddeds();
            embeddedFiles.putAll(copySubPackages());

            writeProperties(new File(vaultDir, "properties.xml"));

            File filterFile = new File(vaultDir, "filter.xml");
            def String filterSrc = project.projectDir.absolutePath+"/"+getExtension().filterSource
            log.error("Filter source absolute path - " + filterSrc)
            File filterSource = new File(filterSrc);
            log.error("Filter file - "+ filterSource)

            if ((filterSource == null) || (!filterSource.exists()))
            {
                DefaultWorkspaceFilter oldFilter = null;
                if (filterFile.exists()) {
                    oldFilter = new DefaultWorkspaceFilter();
                    oldFilter.load(filterFile);

                    if (filters.getSourceAsString().equals(oldFilter.getSourceAsString()))
                        filterFile = null;
                    else {
                        filterFile = new File(vaultDir, "filter-plugin-generated.xml");
                    }
                }
                else if ((filters.getFilterSets().isEmpty()) && (prefix.length() > 0)) {
                    addWorkspaceFilter(prefix);
                }
            }
            else {
                log.info("Merging " + filterSource.getPath() + " with inlined filter specifications.");
                DefaultWorkspaceFilter oldFilter = new DefaultWorkspaceFilter();
                oldFilter.load(filterSource);
                //oldFilter.merge(filters);
                filters.getFilterSets().clear();
                filters.getFilterSets().addAll(oldFilter.getFilterSets());
                if ((filters.getFilterSets().isEmpty()) && (prefix.length() > 0)) {
                    addWorkspaceFilter(prefix);
                }
            }

            if (filterFile != null) {
                FileUtils.fileWrite(filterFile.getAbsolutePath(), filters.getSourceAsString());
            }

            String subDir = project.projectDir.path+"/"+getExtension().builtContentDirectory.replace("/jcr_root","");
            copyFile(subDir+"/META-INF/vault/config.xml", new File(vaultDir, "config.xml"));
            copyFile(subDir+"/META-INF/vault/settings.xml", new File(vaultDir, "settings.xml"));
            copyFile(subDir+"/META-INF/vault/definition/.content.xml", new File(definitionDir, ".content.xml"));
            copyFile(subDir+"/META-INF/vault/package.thumbnail.png", new File(definitionDir, "thumbnail.png"));

            ContentPackageArchiver contentPackageArchiver = new ContentPackageArchiver();
            contentPackageArchiver.addConfiguredManifest(getManifest())
            contentPackageArchiver.addDirectory(new File(workDirectory));
            File builtContentDirectory = new File(project.projectDir.path+"/"+getExtension().builtContentDirectory)
            if (builtContentDirectory.exists() && builtContentDirectory.isDirectory()) {
                contentPackageArchiver.addDirectory(builtContentDirectory, FileUtils.normalize("jcr_root/" + prefix));
            }

            for (Map.Entry entry : embeddedFiles.entrySet()) {
                contentPackageArchiver.addFile((File)entry.getValue(), (String)entry.getKey());
            }

            contentPackageArchiver.setIncludeEmptyDirs(true)
            contentPackageArchiver.setDestFile(finalFile)
            contentPackageArchiver.createArchive()
        }
        catch (Exception ex) {
            log.error("Packaging Exception occured - "+ ex.getMessage() + "\n" + ex.getStackTrace())
            throw new ContentPackagingException(ex.getMessage(), ex);
        }
    }

    private void writeProperties(File propsFile) throws IOException {
        Properties props = new Properties();

        String description = project.properties.description;
        if (description == null) {
            description = project.name;
            if (description == null) {
                description = getExtension().artifactId;
            }
        }
        props.put("description", description);

       // props.putAll(properties);

        props.put("group", getExtension().group);
        props.put("name", project.name);
        props.put("version", getExtension().version);

        props.put("groupId", project.properties.groupId);
        props.put("artifactId", project.properties.artifactId);

//        if (!dependencies.isEmpty()) {
//            props.put("dependencies", Dependency.toString(dependencies));
//        }

        if (!props.containsKey("createdBy")) {
            props.put("createdBy", "GradleBuild");
        }
        DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        props.put("created", DATE_FORMAT.format(new Date()));

        //props.put("requiresRoot", String.valueOf(requiresRoot));
        props.put("path", "/etc/packages/" + getExtension().group + "/" + project.name + ".zip");

        FileOutputStream fos = new FileOutputStream(propsFile);
        props.storeToXML(fos, project.name);
        fos.close();
    }

    private void copyFile(String source, File target)
            throws IOException
    {
        log.error("Inside copyFile for - "+ source)
        if (target.exists()) {
            log.error("Inside target.exists() ")
            return;
        }

        log.error("Target Absolute path - " + target.absolutePath)

        target.getParentFile().mkdirs();

        InputStream ins
        try {
            ins = new FileInputStream(source)
        } catch (Exception e){
            ins = null
        }

        if (ins != null) {
            log.error("input stream not null")
            OutputStream out = null;
            try {
                out = new FileOutputStream(target);
                IOUtil.copy(ins, out);
            } finally {
                IOUtil.close(ins);
                IOUtil.close(out);
            }
        }
    }

    private Map<String, File> copyEmbeddeds() throws IOException, ContentPackagingException {
        Map fileMap = new HashMap()
        def String targetPath = ""
        List<Map<String,String>> embeddeds
        embeddeds = getExtension().embeddeds
        log.error("embeddeds =" + embeddeds)
        for (Iterator i$ = embeddeds.iterator(); i$.hasNext(); ) {
            def Embedded emb = new Embedded((Map<String,String>)i$.next())
            List artifacts = emb.getMatchingArtifacts(project);
            if (artifacts.isEmpty()) {
                if (Boolean.getBoolean(getProject().properties.failOnMissingEmbed.toString())) {
                    throw new ContentPackagingException("Embedded artifact specified " + emb + ", but no matching dependency artifact found. Add the missing dependency or fix the embed definition.");
                }
                log.warn("No matching artifacts for " + emb);
            }
            else
            {
                if ((emb.getDestFileName() != null) && (artifacts.size() > 1)) {
                    log.warn("destFileName defined but several artifacts match for " + emb);
                }

                targetPath = emb.getTarget();
                if (targetPath == null) {
                    targetPath = getProject().properties.embeddedTarget
                    if (targetPath == null) {
                        String loc = prefix.length() == 0 ? "/apps/" : prefix;

                        targetPath = loc + "bundles/install/";
                        log.info("No target path set on " + emb + "; assuming default " + targetPath);
                    }

                }

                targetPath = makeAbsolutePath(targetPath);

                targetPath = "jcr_root/" + targetPath;
                targetPath = FileUtils.normalize(targetPath);

                log.info("Embedding --- " + emb + " ---");
                for (i$ = artifacts.iterator(); i$.hasNext(); ) {
                    File source = i$.next()
                    String destFileName = emb.getDestFileName();

                    if (destFileName == null) {
                        destFileName = source.getName();
                    }
                    String targetPathName = targetPath + destFileName;
                    String targetNodePathName = targetPathName.substring("jcr_root/".length() - 1);

                    fileMap.put(targetPathName, source);
                   // log.info(String.format("Embedding %s (from %s) -> %s", targetPathName));

                    if (emb.isFilter())
                        addWorkspaceFilter(targetNodePathName);
                }
            }
        }
        return fileMap;
    }

    private Map<String, File> copySubPackages() throws IOException {
        Map fileMap = new HashMap();
        for (Iterator i$ = getExtension().subPackages.iterator(); i$.hasNext(); ) {
            def SubPackage pack
            pack = new SubPackage( (Map<String,String>)i$.next())
            List artifacts = pack.getMatchingArtifacts(project);
            if (artifacts.isEmpty()) {
                log.warn("No matching artifacts for " + pack);
            }
            else
            {
                log.info("Embedding --- " + pack + " ---");
                for (i$ = artifacts.iterator(); i$.hasNext(); ) {
                    File source = (File) i$.next();
                    def InputStream iStream = null;
                    def ZipFile zipFile
                    Properties props = new Properties();
                    try {
                        zipFile = new ZipFile(source, 1);
                        ZipEntry e = zipFile.getEntry("META-INF/vault/properties.xml");
                        if (e == null) {
                            log.error("Package does not contain properties.xml");
                            throw new IOException("properties.xml missing");
                        }
                        iStream = zipFile.getInputStream(e)
                        props.loadFromXML(zipFile.getInputStream(e));
                    } finally {
                        IOUtil.close(iStream);
                        if (zipFile != null) {
                            zipFile.close();
                        }
                    }
                    PackageId pid = new PackageId(props.getProperty("group"), props.getProperty("name"), props.getProperty("version"));

                    String targetNodePathName = pid.getInstallationPath() + ".zip";
                    String targetPathName = "jcr_root" + targetNodePathName;

                    fileMap.put(targetPathName, source);
                    log.info("Embedding " + source.name + " -> " + targetPathName);
                    if (pack.isFilter())
                        addWorkspaceFilter(targetNodePathName);
                }
            }
        }
        return fileMap;
    }

    private void addWorkspaceFilter(String filterRoot) {
        filters.add(new PathFilterSet(filterRoot));
    }

    private String makeAbsolutePath(String relPath)
    {
        String absPath;
        if (!relPath.startsWith("/")) {
            absPath = (prefix.length() == 0 ? "/" : prefix) + relPath;
            log.info("Relative path resolved to " + absPath);
        } else {
            absPath = relPath;
        }

        return absPath;
    }

    private Manifest getManifest(){
        Manifest mf = new Manifest()
        Map<String, String> mAttr = project.extensions.getByName(ContentPackagePluginConstant.CONTENT_PACKAGE_MANIFEST).properties.attributes
        mAttr.each{ key, value ->

            Manifest.Attribute attr = new Manifest.Attribute()
            attr.setName(key)
            attr.setValue(value)
            mf.addConfiguredAttribute(attr)
        }

        return mf
    }
}
