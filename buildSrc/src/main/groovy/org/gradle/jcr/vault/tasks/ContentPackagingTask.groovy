package org.gradle.jcr.vault.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class ContentPackagingTask extends AbstractPackagingTask {

    private static final Logger log = LoggerFactory.getLogger(ContentPackagingTask.class);

    @TaskAction
    public void buildPackage(){

        super.buildPackage()
    }
}
