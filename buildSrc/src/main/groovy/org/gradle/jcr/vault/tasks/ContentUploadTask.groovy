package org.gradle.jcr.vault.tasks

import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.HttpStatus
import org.apache.commons.httpclient.UsernamePasswordCredentials
import org.apache.commons.httpclient.auth.AuthScope
import org.apache.commons.httpclient.methods.PostMethod
import org.apache.commons.httpclient.methods.multipart.FilePart
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity
import org.apache.commons.httpclient.methods.multipart.Part
import org.apache.commons.httpclient.methods.multipart.StringPart
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import java.util.regex.Matcher
import java.util.regex.Pattern
import org.gradle.api.file.FileTree
import org.gradle.jcr.vault.exception.ContentPackagingException
import org.gradle.jcr.vault.plugin.ContentPackagePluginConstant
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.codehaus.plexus.util.IOUtil
import org.codehaus.plexus.util.xml.Xpp3Dom
import org.codehaus.plexus.util.xml.Xpp3DomBuilder
import org.codehaus.plexus.util.xml.pull.XmlPullParserException

/**
 * Created by ameesh.trikha on 30/12/2014.
 */
class ContentUploadTask extends DefaultTask {

  private static final Logger logger = LoggerFactory.getLogger(ContentUploadTask.class)

    public File packageFile

    @TaskAction
    public void uploadContentPackage()
            throws ContentPackagingException
    {
        String fileName = project.getExtensions().findByName(ContentPackagePluginConstant.CONTENT_PACKAGE_PLUGIN_EXTENSION).properties.finalName
        def FileTree fileTree = project.fileTree(project.projectDir.absolutePath).matching { include("**/*"+fileName+"*.zip")}
        fileTree.each {File pkg ->
            logger.error("found file - "+ pkg)
            this.packageFile = pkg
        }
        uploadPackage();
    }

    private void uploadPackage() throws ContentPackagingException {
        String task = project.properties.install ? "Installing" : "Uploading";
        logger.info(task + " " + this.name + " (" + this.packageFile + ") to " + getTargetURL());
        try
        {
            ArrayList parts = new ArrayList();
            parts.add(new FilePart("file", this.packageFile));

            if (project.properties.install) {
                parts.add(new StringPart("install", "true"));
            }

            Xpp3Dom result = postRequest(null, parts);
            if (checkStatus(result)) {
                Xpp3Dom pkg = getNestedChild(result, "response/data/package");

                logPackage(pkg);

                if (false) {
                    Xpp3Dom log = getNestedChild(result, "response/data/log");

                    if (log != null)
                        logger.info(getText(log, null));
                }
            }
            else if (false) {
                throw new ContentPackagingException("Error while installing package. Check log for details.");
            }
        }
        catch (FileNotFoundException fnfe)
        {
        }
    }

    protected String getTargetURL() {

        return project.extensions.findByName(ContentPackagePluginConstant.CONTENT_PACKAGE_PLUGIN_EXTENSION).properties.targetURL
    }

    protected Xpp3Dom postRequest(String command, Collection<Part> parameters)
            throws ContentPackagingException
    {
        if (parameters == null) {
            parameters = new ArrayList();
        }
        if (command != null) {
            parameters.add(new StringPart("cmd", command));
        }
        if (parameters.isEmpty()) {
            logger.error("Not POSTing to " + getTargetURL() + ": Missing parameters");

            return null;
        }

        PostMethod filePost = new PostMethod(getTargetURL());
        filePost.getParams().setBooleanParameter("http.protocol.expect-continue", true);

        filePost.setRequestHeader("referer", "about:blank");
        try
        {
            Part[] parts = new Part[parameters.size()];
            parameters.toArray(parts);

            filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));

            HttpClient client = new HttpClient();
            client.getParams().setAuthenticationPreemptive(true);
            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(project.properties.crxUsername, project.properties.crxPassword);

            client.getState().setCredentials(AuthScope.ANY, credentials);

            client.getHttpConnectionManager().getParams().setConnectionTimeout(50000);

            int status = client.executeMethod(filePost);
            StringWriter out;
            if (status == 200) { out = new StringWriter();
                def InputStreamReader iStream = new InputStreamReader(filePost.getResponseBodyAsStream(), filePost.getRequestCharSet());

                IOUtil.copy(iStream, out);
                String ret = out.toString();
                Xpp3Dom dom;
                try { dom = Xpp3DomBuilder.build(new StringReader(ret));
                } catch (XmlPullParserException e)
                {
                    Matcher m = Pattern.compile("<log>(.*)</log>", 32).matcher(ret);
                    StringBuffer result = new StringBuffer();
                    while (m.find()) {
                        String log = "<log><![CDATA[" + m.group(1) + "]]></log>";
                        m.appendReplacement(result, Matcher.quoteReplacement(log));
                    }
                    m.appendTail(result);
                    ret = result.toString();
                    dom = Xpp3DomBuilder.build(new StringReader(ret));
                }
                return dom;
            }

            logger.error("Request to " + getTargetURL() + " failed, response=" + HttpStatus.getStatusText(status));

            return null;
        }
        catch (Exception ex)
        {
            throw new ContentPackagingException(ex.getMessage(), ex);
        }
        finally
        {
            filePost.releaseConnection();
        }
    }

    protected Xpp3Dom getNestedChild(Xpp3Dom element, String path)
    {
        String[] parts = path.split("/");

        Xpp3Dom current = element;
        for (int i = 0; (i < parts.length) && (current != null); i++) {
            current = current.getChild(parts[i]);
        }

        return current;
    }

    protected String getText(Xpp3Dom parent, String childName) {
        Xpp3Dom child = childName == null ? parent : parent.getChild(childName);

        if (child != null) {
            return child.getValue();
        }

        return parent.getAttribute(childName);
    }

    protected boolean checkStatus(Xpp3Dom result)
    {
        if (result == null) {
            return false;
        }

        Xpp3Dom status = getNestedChild(result, "response/status");
        if (status == null) {
            logger.error("Missing response status information in response: " + result);

            return false;
        }

        String code = getText(status, "code");
        if ("200".equals(code)) {
            logger.debug("Request succeeded");
            return true;
        }

        logger.error("Request failed: " + getText(status, null) + " (" + getText(status, "code") + ")");

        return false;
    }

    protected void logPackage(Xpp3Dom pkg)
    {
        if (pkg != null) {
            StringBuilder buf = new StringBuilder();

            buf.append(getText(pkg, "name"));
            buf.append(", ");
            buf.append(getText(pkg, "version"));
            buf.append(" (");
            buf.append(getText(pkg, "size"));
            buf.append(" bytes)");

            logger.info(buf.toString());

         /*   if (isVerbose()) {
                logPackageDate(pkg, "Created ", "created");
                logPackageDate(pkg, "Modified", "lastModified");
                logPackageDate(pkg, "Unpacked", "lastUnpacked");
                logger.info("");
            }*/
        }
    }

    private void logPackageDate(Xpp3Dom pkg, String label, String kind)
    {
        String user = getText(pkg, kind + "By");

        StringBuilder builder = new StringBuilder();
        builder.append("    ").append(label).append(": ");
        if ((user != null) && (!user.equals("null")))
            builder.append(getText(pkg, kind)).append(" by ").append(user);
        else {
            builder.append("-");
        }
        logger.info(builder);
    }
}
