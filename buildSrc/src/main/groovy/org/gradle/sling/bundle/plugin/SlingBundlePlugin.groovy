package org.gradle.sling.bundle.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.sling.bundlesupport.tasks.BundleInstallTask
import org.gradle.sling.bundlesupport.tasks.BundleUninstallTask

/**
 * Created by ameesh.trikha on 28/12/2014.
 */
public class SlingBundlePlugin implements Plugin<Project> {

    public void apply(Project project){
        project.extensions.create(SlingPluginConstants.SLING_PLUGIN_BUNDLE_EXTENSION , SlingBundlePluginExtension)
        project.task(SlingPluginConstants.BUNDLE_INSTALL_TASK, type: BundleInstallTask)
        project.task(SlingPluginConstants.BUNDLE_UNINSTALL_TASK, type: BundleUninstallTask)
    }
}