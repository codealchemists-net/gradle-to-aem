package org.gradle.sling.bundle.plugin

/**
 * Created by ameesh.trikha on 28/12/2014.
 */
class SlingBundlePluginExtension {
    /**
     * The URL of the running Sling instance.
     *
     * @parameter expression="${sling.url}"
     *            default-value="http://localhost:8080/system/console"
     * @required
     */
    def String slingUrl;

    /**
     * An optional url suffix which will be appended to the <code>sling.url</code>
     * for use as the real target url. This allows to configure different target URLs
     * in each build file, while using the same common <code>sling.url</code> in a parent
     * build file (eg. <code>sling.url=http://localhost:8080</code> and
     * <code>sling.urlSuffix=/project/specific/path</code>). This is typically used
     * in conjunction with a HTTP PUT (<code>sling.usePut=true</code>).
     *
     * @parameter expression="${sling.urlSuffix}"
     */
    def String slingUrlSuffix;

    /**
     * If a simple HTTP PUT should be used instead of the standard POST to the
     * felix console. In the <code>uninstall</code> goal, a HTTP DELETE will be
     * used.
     *
     * @parameter expression="${sling.usePut}" default-value="false"
     * @required
     */
    def boolean usePut;

    /**
     * The content type / mime type used for the HTTP PUT (if
     * <code>sling.usePut=true</code>).
     *
     * @parameter expression="${sling.mimeType}"
     *            default-value="application/java-archive"
     * @required
     */
    def String mimeType;

    /**
     * The user name to authenticate at the running Sling instance.
     *
     * @parameter expression="${sling.user}" default-value="admin"
     * @required
     */
    def String user;

    /**
     * The password to authenticate at the running Sling instance.
     *
     * @parameter expression="${sling.password}" default-value="admin"
     * @required
     */
    def String password;

    /**
     * The startlevel for the uploaded bundle
     *
     * @parameter expression="${sling.bundle.startlevel}" default-value="20"
     * @required
     */
    def String bundleStartLevel;

    /**
     * Whether to start the uploaded bundle or not
     *
     * @parameter expression="${sling.bundle.start}" default-value="true"
     * @required
     */
    def boolean bundleStart;

    /**
     * Whether to refresh the packages after installing the uploaded bundle
     *
     * @parameter expression="${sling.refreshPackages}" default-value="true"
     * @required
     */
    def boolean refreshPackages;

    def String bundleFileName;
}
