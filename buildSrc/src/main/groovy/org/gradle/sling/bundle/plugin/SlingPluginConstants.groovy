package org.gradle.sling.bundle.plugin

/**
 * Created by ameesh.trikha on 28/12/2014.
 */
final class SlingPluginConstants {

    public static final String SLING_PLUGIN_BUNDLE_EXTENSION = "slingBundleExtension"

    public static final String BUNDLE_FILE_NAME_PARAM = "bundleFileName"

    public static final String BUNDLE_INSTALL_TASK = "autoInstallBundle"

    public static final String BUNDLE_INSTALL_PUBLISH_TASK = "autoInstallBundlePublish"

    public static final String BUNDLE_UNINSTALL_TASK = "uninstallBundle"
}
