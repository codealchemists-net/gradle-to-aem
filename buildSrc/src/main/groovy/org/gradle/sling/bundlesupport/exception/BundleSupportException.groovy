package org.gradle.sling.bundlesupport.exception;

/**
 * Created by ameesh.trikha on 27/12/2014.
 */
public class BundleSupportException extends Exception {
    public BundleSupportException(String msg) {
        super(msg);
    }

    public BundleSupportException(String msg, Exception ex) {
        super(msg , ex);
    }
}
