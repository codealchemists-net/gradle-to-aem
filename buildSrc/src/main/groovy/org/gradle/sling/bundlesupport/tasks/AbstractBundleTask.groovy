package org.gradle.sling.bundlesupport.tasks
import org.apache.commons.httpclient.Credentials
import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.UsernamePasswordCredentials
import org.apache.commons.httpclient.auth.AuthScope
import org.gradle.api.DefaultTask
import org.gradle.sling.bundle.plugin.SlingBundlePluginExtension
import org.gradle.sling.bundle.plugin.SlingPluginConstants
/**
 * Created by ameesh.trikha on 29/12/2014.
 */
abstract class AbstractBundleTask extends DefaultTask {

    /**
     * Returns the combination of <code>sling.url</code> and
     * <code>sling.urlSuffix</code>.
     */
    protected String getTargetURL() {
        String targetURL = getExtensionPropAsString("slingUrl");
        String slingUrlSuffix = getExtensionPropAsString("slingUrlSuffix");
        if (slingUrlSuffix != null) {
            targetURL += slingUrlSuffix;
        }
        return targetURL;
    }

    SlingBundlePluginExtension getExtension(){
        SlingBundlePluginExtension extension = (SlingBundlePluginExtension) getProject().getExtensions().getByName(SlingPluginConstants.SLING_PLUGIN_BUNDLE_EXTENSION);
        return extension;
    }

    String getExtensionPropAsString(String propertyName) {
        Object property = getExtension().getProperty(propertyName);
        if(null!=property){
            return (String) getExtension().getProperty(propertyName);
        }
        return null;
    }

    boolean getExtensionPropAsBoolean(String propertyName){
        Object property = getExtension().getProperty(propertyName);
        if(null!=property){
            return (Boolean) getExtension().getProperty(propertyName);
        }
        return false;
    }

    /**
     * Get the http client
     */
    protected HttpClient getHttpClient() {
        final HttpClient client = new HttpClient();
        client.getHttpConnectionManager().getParams().setConnectionTimeout(
                5000);

        // authentication stuff
        client.getParams().setAuthenticationPreemptive(true);
        Credentials defaultCreds = new UsernamePasswordCredentials(getExtensionPropAsString("user"),
                getExtensionPropAsString("password"));
        client.getState().setCredentials(AuthScope.ANY, defaultCreds);

        return client;
    }

    /**
     * Returns the URL for PUT or DELETE by appending the filename to the
     * targetURL.
     */
    String getPutURL(String targetURL, String fileName) {
        return targetURL + (targetURL.endsWith("/") ? "" : "/") + fileName;
    }

    String getBundleFileName() {
        return this.getExtension().getProperty(SlingPluginConstants.BUNDLE_FILE_NAME_PARAM).toString();
    }
}
