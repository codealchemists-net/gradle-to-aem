package org.gradle.sling.bundlesupport.tasks

import org.apache.commons.httpclient.HttpStatus
import org.apache.commons.httpclient.methods.FileRequestEntity
import org.apache.commons.httpclient.methods.PostMethod
import org.apache.commons.httpclient.methods.PutMethod
import org.apache.commons.httpclient.methods.multipart.FilePart
import org.apache.commons.httpclient.methods.multipart.FilePartSource
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity
import org.apache.commons.httpclient.methods.multipart.Part
import org.apache.commons.httpclient.methods.multipart.StringPart
import org.gradle.api.Task
import org.gradle.api.tasks.TaskAction
import org.gradle.sling.bundlesupport.exception.BundleSupportException
import org.gradle.sling.bundlesupport.util.BundleSupportUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by ameesh.trikha on 27/12/2014.
 */
public class BundleInstallTask extends AbstractBundleTask {

    private static final Logger log = LoggerFactory.getLogger(BundleInstallTask.class);

    @TaskAction
    public void installBundle() throws BundleSupportException {

        // get the file to upload
        String bundleFileName = getBundleFileName();
        log.error("Bundle File name - " + bundleFileName);

        // only upload if packaging as an osgi-bundle
        File bundleFile = new File(bundleFileName);
        String bundleName = BundleSupportUtil.getBundleSymbolicName(bundleFile);
        if (bundleName == null) {
            log.info(bundleFile + " is not an OSGi Bundle, not uploading");
            return;
        }

        String targetURL = getTargetURL();

        log.info(
                "Installing Bundle " + bundleName + "(" + bundleFile + ") to "
                        + targetURL + " via "+ (getExtensionPropAsBoolean("usePut") ? "PUT" : "POST"));

        if (getExtensionPropAsBoolean("usePut")) {
            put(targetURL, bundleFile);
        } else {
            post(targetURL, bundleFile);
        }


    }

    protected void post(String targetURL, File file)
            throws BundleSupportException {
        boolean failOnError = false;
        // append pseudo path after root URL to not get redirected for nothing

        log.error("Target URL - " + targetURL);
        log.error("File name - " + file);
        PostMethod filePost = new PostMethod(targetURL + "/install");

        try {

            List<Part> partList = new ArrayList<Part>();
            partList.add(new StringPart("action", "install"));
            partList.add(new StringPart("_noredir_", "_noredir_"));
            partList.add(new FilePart("bundlefile", new FilePartSource(
                    file.getName(), file)));
            partList.add(new StringPart("bundlestartlevel", getExtensionPropAsString("bundleStartLevel")));

            if (getExtensionPropAsBoolean("bundleStart")) {
                partList.add(new StringPart("bundlestart", "start"));
            }

            if (getExtensionPropAsBoolean("refreshPackages")) {
                partList.add(new StringPart("refreshPackages", "true"));
            }

            Part[] parts = partList.toArray(new Part[partList.size()]);

            filePost.setRequestEntity(new MultipartRequestEntity(parts,
                    filePost.getParams()));

            int status = getHttpClient().executeMethod(filePost);
            if (status == HttpStatus.SC_OK) {
                log.info("Bundle installed");
            } else {
                String msg = "Installation failed, cause: "
                +HttpStatus.getStatusText(status);
                if (failOnError) {
                    throw new BundleSupportException(msg);
                } else {
                    log.error(msg);
                }
            }
        } catch (Exception ex) {
            log.error("Error Message" + ex.getMessage() + ex.getStackTrace());
            throw new BundleSupportException("Installation on " + targetURL
                    + " failed, cause: " + ex.getMessage(), ex);
        } finally {
            filePost.releaseConnection();
        }
    }

    protected void put(String targetURL, File file) throws BundleSupportException {

        PutMethod filePut = new PutMethod(getPutURL(targetURL, file.getName()));
        try {
            filePut.setRequestEntity(new FileRequestEntity(file, "application/java-archive"));

            int status = getHttpClient().executeMethod(filePut);
            if (status >= HttpStatus.SC_OK && status < HttpStatus.SC_MULTIPLE_CHOICES) {
                log.info("Bundle installed");
            } else {
                String msg = "Installation failed, cause: "
                +HttpStatus.getStatusText(status);
                if (false) {
                    throw new BundleSupportException(msg);
                } else {
                    log.error(msg);
                }
            }
        } catch (Exception ex) {
            throw new BundleSupportException("Installation on " + targetURL
                    + " failed, cause: " + ex.getMessage(), ex);
        } finally {
            filePut.releaseConnection();
        }
    }
}
