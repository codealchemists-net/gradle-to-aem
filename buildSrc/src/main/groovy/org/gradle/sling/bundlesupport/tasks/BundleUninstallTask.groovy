package org.gradle.sling.bundlesupport.tasks
import org.apache.commons.httpclient.HttpStatus
import org.apache.commons.httpclient.methods.DeleteMethod
import org.apache.commons.httpclient.methods.PostMethod
import org.gradle.api.tasks.TaskAction
import org.gradle.sling.bundlesupport.exception.BundleSupportException
import org.gradle.sling.bundlesupport.util.BundleSupportUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
/**
 * Created by ameesh.trikha on 29/12/2014.
 */
class BundleUninstallTask extends AbstractBundleTask {

    static Logger log = LoggerFactory.getLogger(BundleUninstallTask.class);

    @TaskAction
    public void uninstallBundle() throws BundleSupportException {
        // only upload if packaging as an osgi-bundle
        // get the file to upload
        String bundleFileName = getBundleFileName();
        final File bundleFile = new File(bundleFileName);
        final String bundleName = BundleSupportUtil.getBundleSymbolicName(bundleFile);
        if (bundleName == null) {
            log.info(bundleFile + " is not an OSGi Bundle, not uploading");
            return;
        }

        String targetURL = getTargetURL();

        log.info(
                "Unistalling Bundle " + bundleName + ") from "
                        + targetURL + " via " + (usePut ? "DELETE" : "POST"));

        if (getExtensionPropAsBoolean("usePut")) {
            delete(targetURL, bundleFile);
        } else {
            post(targetURL, bundleName);
        }
    }

    protected void delete(String targetURL, File file)
            throws BundleSupportException {

        final DeleteMethod delete = new DeleteMethod(getPutURL(targetURL, file.getName()));

        try {

            int status = getHttpClient().executeMethod(delete);
            if (status >= HttpStatus.SC_OK && status < HttpStatus.SC_MULTIPLE_CHOICES) {
                log.info("Bundle uninstalled");
            } else {
                log.error(
                        "Uninstall failed, cause: "
                                + HttpStatus.getStatusText(status));
            }
        } catch (Exception ex) {
            throw new BundleSupportException("Uninstall from " + targetURL
                    + " failed, cause: " + ex.getMessage(), ex);
        } finally {
            delete.releaseConnection();
        }
    }

    protected void post(String targetURL, String symbolicName)
            throws BundleSupportException {
        final PostMethod post = new PostMethod(targetURL + "/bundles/" + symbolicName);
        post.addParameter("action", "uninstall");

        try {

            int status = getHttpClient().executeMethod(post);
            if (status == HttpStatus.SC_OK) {
                log.info("Bundle uninstalled");
            } else {
                log.error(
                        "Uninstall failed, cause: "
                                + HttpStatus.getStatusText(status));
            }
        } catch (Exception ex) {
            throw new BundleSupportException("Uninstall from " + targetURL
                    + " failed, cause: " + ex.getMessage(), ex);
        } finally {
            post.releaseConnection();
        }
    }
}
