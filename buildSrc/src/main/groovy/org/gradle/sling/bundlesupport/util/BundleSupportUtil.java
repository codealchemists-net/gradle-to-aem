package org.gradle.sling.bundlesupport.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

/**
 * Created by ameesh.trikha on 27/12/2014.
 */
public final class BundleSupportUtil {

    static Logger log = LoggerFactory.getLogger(BundleSupportUtil.class);

    /**
     * Returns the symbolic name of the given bundle. If the
     * <code>jarFile</code> does not contain a manifest with a
     * <code>Bundle-SymbolicName</code> header <code>null</code> is
     * returned. Otherwise the value of the <code>Bundle-SymbolicName</code>
     * header is returned.
     * <p>
     * This method may also be used to check whether the file is a bundle at all
     * as it is assumed, that only if the file contains an OSGi bundle will the
     * <code>Bundle-SymbolicName</code> manifest header be set.
     *
     * @param jarFile The file providing the bundle whose symbolic name is
     *            requested.
     * @return The bundle's symbolic name from the
     *         <code>Bundle-SymbolicName</code> manifest header or
     *         <code>null</code> if no manifest exists in the file or the
     *         header is not contained in the manifest. However, if
     *         <code>null</code> is returned, the file may be assumed to not
     *         contain an OSGi bundle.
     */
    public static String getBundleSymbolicName(File jarFile) {

        if (!jarFile.exists()) {
            return null;
        }
log.error("Inside getBundleSymbolicName");
        JarFile jaf = null;
        try {
            jaf = new JarFile(jarFile);
            Manifest manif = jaf.getManifest();
            if (manif == null) {
                log.debug(
                        "getBundleSymbolicName: Missing manifest in " + jarFile);
                return null;
            }

            String symbName = manif.getMainAttributes().getValue(
                    "Bundle-SymbolicName");
            if (symbName == null) {
                log.debug(
                        "getBundleSymbolicName: No Bundle-SymbolicName in "
                                + jarFile);
                return null;
            }

            return symbName;
        } catch (IOException ioe) {
            log.warn("getBundleSymbolicName: Problem checking " + jarFile,
                    ioe);
        } finally {
            if (jaf != null) {
                try {
                    jaf.close();
                } catch (IOException ignore) {
                    // don't care
                }
            }
        }

        // fall back to not being a bundle
        return null;
    }
}
